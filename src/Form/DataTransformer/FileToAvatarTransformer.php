<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Form\DataTransformer;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Utils\Symfony;
use Symfony\Component\Form\DataTransformerInterface;

class FileToAvatarTransformer implements DataTransformerInterface
{

    protected $mediaProviderService;
    protected $user;

    public function __construct($user, $mediaProviderService)
    {
        $this->mediaProviderService = $mediaProviderService;
        $this->user = $user;
    }

    public function transform($file)
    {
        return $file;
    }

    public function reverseTransform($file)
    {
        if (!$file) {
            return $this->user->getAvatar();
        }

        if (!$file->getFile()) {
            return $this->user->getAvatar();
        }

        $id = $this->user->getId();
        $file = $file->getFile();
        $root = Symfony::getRoot();
        $extension = ".".$file->guessExtension();
        $path = "/uploads/images/user";
        $filename = $id.$extension;

        $media = $this->mediaProviderService->buildMedia($file, 'image', $this->user->getAvatar());
        $media->setPath("$path");
        $media->setName($filename);
        $media->upload(
            "$root/web$path",
            $filename
        );

        return $media;
    }
}
