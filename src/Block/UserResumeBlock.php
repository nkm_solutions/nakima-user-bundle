<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class UserResumeBlock extends \Nakima\AdminBundle\Block\AbstractResumeBlock
{

    public function getName()
    {
        return 'user_resume_block';
    }

    function getIconClass()
    {
        return 'users';
    }

    function getTitle()
    {
        $userRepo = $this->getContainer()->get('doctrine')->getRepository('UserBundle:User');

        return count($userRepo->findAll());
    }

    function getSubTitle()
    {
        return "Users";
    }

    function getLinkLabel()
    {
        return "Manage Users";
    }

    function getColor()
    {
        return 'gray';
    }

    function getDestination()
    {
        return '...';
    }

    function getPath()
    {
        return "/User/User/list";
    }
}
