<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Block;

use Nakima\AdminBundle\Block\EntityBlock;

class UserBlock extends EntityBlock
{

    public function getTemplate()
    {

        if ($this->action == "list") {
            return "NakimaUserBundle:Core:$this->action.html.twig";
        }

        return parent::getTemplate();
    }
}
