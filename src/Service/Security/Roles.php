<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Service\Security;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\Security\Core\Role\RoleHierarchy;

class Roles
{

    private $rolesHierarchy;

    private $roles;

    public function __construct($rolesHierarchy)
    {
        $this->rolesHierarchy = new RoleHierarchy($rolesHierarchy);
    }

    public function getHierarchy()
    {
        return $this->rolesHierarchy;
    }
}
