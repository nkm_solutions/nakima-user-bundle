<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Service;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class PasswordRecoverEmail
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function sendInvite($controller, $me, $invitedUser)
    {

        Mailer::newInstance(
            "You have a ".$this->container->getParameter("nakima")['title']." invitation.",
            $this->container->getParameter("nakima")['email'],
            $user->getEmail(),
            $this->container->getParameter("nakima_user")['emails']['invitation'],
            ['me' => $me, 'invitedUser' => $invitedUser]
        );
        Mailer::send();
    }

    public function send($controller, $user)
    {

        Mailer::newInstance(
            $this->container->getParameter("nakima")['title']." Recover Password",
            $this->container->getParameter("nakima")['email'],
            $user->getEmail(),
            $this->container->getParameter("nakima_user")['emails']['password_recover'],
            ['user' => $user]
        );
        Mailer::send();
    }
}
