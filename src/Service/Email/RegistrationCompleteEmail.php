<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Service\Email;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Mailer\Mailer;
use Nakima\CoreBundle\Service\Email\Email;


class RegistrationCompleteEmail
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function send($controller, $user)
    {

        Mailer::newInstance(
            $this->container->getParameter("nakima")['title']." account activated",
            $this->container->getParameter("nakima")['email'],
            $user->getEmail(),
            $this->container->getParameter("nakima_user")['emails']['register_complete'],
            ['user' => $user]
        );
        Mailer::send();

    }
}
