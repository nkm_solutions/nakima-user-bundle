<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Service\Email;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Mailer\Mailer;
use Nakima\CoreBundle\Service\Email\Email;


class PasswordRecoverEmail
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function send($controller, $user)
    {

        Mailer::newInstance(
            $this->container->getParameter("nakima")['title']." Recover Password",
            $this->container->getParameter("nakima")['email'],
            $user->getEmail(),
            $this->container->getParameter("nakima_user")['emails']['password_recover'],
            ['user' => $user]
        );
        Mailer::send();

    }
}
