<?php
declare(strict_types=1);
namespace Nakima\UserBundle\DependencyInjection;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

    public function getConfigTreeBuilder() {

        $treeBuilder = new TreeBuilder;
        $rootNode = $treeBuilder->root('nakima_user');

        $rootNode
            ->children()
                ->arrayNode('templates')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('login')->defaultValue('NakimaUserBundle:User:login.html.twig')->end()
                        ->scalarNode('register')->defaultValue('NakimaUserBundle:User:register.html.twig')->end()
                        ->scalarNode('register_complete')->defaultValue('NakimaUserBundle:User:register_complete.html.twig')->end()
                        ->scalarNode('register_validate')->defaultValue('NakimaUserBundle:User:register_validate.html.twig')->end()
                        ->scalarNode('password_recover')->defaultValue('NakimaUserBundle:User:password_recover.html.twig')->end()
                        ->scalarNode('password_recover_complete')->defaultValue('NakimaUserBundle:User:password_recover_complete.html.twig')->end()
                        ->scalarNode('password_new')->defaultValue('NakimaUserBundle:User:password_new.html.twig')->end()
                        // ADMIN
                        ->scalarNode('admin_user_list')->defaultValue('NakimaUserBundle:Core:list.html.twig')->end()
                    ->end()
                ->end()
                ->arrayNode("stylesheets")
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('emails')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('register_validation')->defaultValue('NakimaUserBundle:Email:registration.html.twig')->end()
                        ->scalarNode('register_complete')->defaultValue('NakimaUserBundle:Email:registration_complete.html.twig')->end()
                        ->scalarNode('password_recover')->defaultValue('NakimaUserBundle:Email:password_recover.html.twig')->end()
                        ->scalarNode('password_recover_complete')->defaultValue('NakimaUserBundle:Email:password_recover_complete.html.twig')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

}
