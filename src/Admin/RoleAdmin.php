<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class RoleAdmin extends Admin
{

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('role');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('role');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('role');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }
}
