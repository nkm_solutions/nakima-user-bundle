<?php
declare(strict_types=1);
namespace Nakima\UserBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use UserBundle\Entity\Gender;

class LoadGenderData extends Fixture
{

    public function loadProd(): void
    {
        $role = new Gender();
        $role->setName('MALE');
        $this->persist($role, 'gender-male');

        $role = new Gender();
        $role->setName('FEMALE');
        $this->persist($role, 'gender-female');
    }
}
