<?php
declare(strict_types=1);
namespace Nakima\UserBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use UserBundle\Entity\Role;

class LoadRoleData extends Fixture
{

    public function loadProd(): void
    {
        $role = new Role();
        $role->setRole('ROLE_ADMIN');
        $this->persist($role, 'role-admin');

        $role = new Role();
        $role->setRole('ROLE_USER');
        $this->persist($role, 'role-user');

        $role = new Role();
        $role->setRole('ROLE_SUPER_ADMIN');
        $this->persist($role, 'role-super-admin');

        $role = new Role();
        $role->setRole('ROLE_MAINTENANCE');
        $this->persist($role, 'role-maintenance');

        $role = new Role();
        $role->setRole('ROLE_DUMMY');
        $this->persist($role, 'role-dummy');

    }
}
