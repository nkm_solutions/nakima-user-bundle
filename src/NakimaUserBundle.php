<?php
declare(strict_types=1);
namespace Nakima\UserBundle;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaUserBundle extends Bundle
{
}
