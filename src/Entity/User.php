<?php
declare(strict_types=1);

namespace Nakima\UserBundle\Entity;

/**
 * @author xgonzalez@nakima.e
 * @since 1.0
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Exception\AccessDeniedException;
use Nakima\CoreBundle\Exception\BadPasswordsException;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\Utils\Time\DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 * @HasLifecycleCallbacks()
 */
class User extends BaseEntity implements UserInterface, \Serializable
{

    /**
     * @Column(type="string", length=20, unique=true)
     * @Assert\Length(min=3)
     * @Assert\NotNull()
     */
    protected $username;

    /**
     * @Column(type="string", length=64)
     * @Assert\Length(max=64)
     * @Assert\NotNull()
     */
    protected $password;

    /**
     * @Column(type="string", length=254, unique=true)
     * @Assert\Email()
     * @Assert\NotNull()
     */
    protected $email;

    /**
     * @Column(type="nakima_datetime", nullable=false)
     * @Assert\NotNull()
     */
    protected $createdAt;

    /**
     * @Column(type="string", length=64, nullable=true, unique=true)
     * @Assert\Length(min=64, max=64)
     */
    protected $recoverPasswordToken;

    /**
     * @Column(type="nakima_datetime", nullable=true)
     */
    protected $recoverPasswordTokenCreatedAt;

    /**
     * @Column(type="string", length=64, nullable=true, unique=true)
     * @Assert\Length(min=64, max=64)
     */
    protected $registerToken;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $enabled;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $deleted;

    /**
     * @ManyToMany(targetEntity="UserBundle\Entity\Role")
     * @JoinTable(
     *     name="_user_roles",
     *     joinColumns={
     *         @JoinColumn(name="user_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="role_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $roles;

    /**
     * @ManyToMany(targetEntity="UserBundle\Entity\Group")
     * @JoinTable(
     *     name="_user_groups",
     *     joinColumns={
     *         @JoinColumn(name="user_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="group_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $groups;

    /**
     * @OneToOne(targetEntity="MediaBundle\Entity\Media", cascade={"all"})
     * @JoinColumn(referencedColumnName="id")
     */
    protected $avatar;

    /**
     * @ManyToOne(targetEntity="UserBundle\Entity\Gender", cascade={"persist"})
     * @JoinColumn(referencedColumnName="id", nullable=false)
     */
    protected $gender;


    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->username;
    }

    public function __construct()
    {
        $this->roles = new ArrayCollection;
        $this->groups = new ArrayCollection;
        $this->setEnabled(false);
        $this->setDeleted(false);
        $this->setCreatedAt(new DateTime);

        //$this->setGender(\UserBundle\Entity\Gender::load("UNKNOWN"));
    }

    public function __toArray(array $options = []): array
    {
        return [
            'avatar' => Doctrine::toArray($this->getAvatar()),
            'createdAt' => $this->getCreatedAt()->format("U"),
            'deleted' => $this->getDeleted(),
            'email' => $this->getEmail(),
            'enabled' => $this->getEnabled(),
            'gender' => Doctrine::toArray($this->getGender()),
            'id' => $this->getId(),
            'username' => $this->getUsername(),
        ];
    }

    /**
     * @PrePersist
     */
    public function prePersist()
    {
        if (!$this->getAvatar()) {
            $media = new \MediaBundle\Entity\Media;
            $media->setPath("/bundles/nakimauser/img");
            $media->setProvider(\MediaBundle\Entity\MediaProvider::IMAGE());
            $media->setName('default_avatar.png');
            $media->setMime('image/png');
            $media->setSize(16000);

            $this->setAvatar($media);
        }
    }

    public function __validate($context, $payload)
    {
        $user = Doctrine::getRepo("UserBundle:User")->findOneByEmail($this->email);
        if ($user && $user->getId() != $this->id) {
            $context->buildViolation(Symfony::getTranslator()->trans('nakima_user_existing_email', [], 'nakima_user'))
                ->atPath('email')
                ->addViolation();
        }

        $user = Doctrine::getRepo("UserBundle:User")->findOneByUsername($this->username);
        if ($user && $user->getId() != $this->id) {
            $context->buildViolation(
                Symfony::getTranslator()->trans(
                    'nakima_user_existing_username',
                    [],
                    'nakima_user'
                )
            )
                ->atPath('username')
                ->addViolation();
        }
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/


    public function setUsername($username)
    {
        if (!$username) {
            return;
        }
        $this->username = $username;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {

        if (!$password) {
            return;
        }

        if ($password !== $this->password) {
            $container = \Nakima\CoreBundle\Utils\Symfony::getContainer();
            $encoder = $container->get('security.password_encoder');
            $password = $encoder->encodePassword($this, $password);
        }

        $this->password = $password;

        return $this;
    }

    public function checkPassword($password)
    {
        $container = \Nakima\CoreBundle\Utils\Symfony::getContainer();
        $encoder = $container->get('security.password_encoder');

        return $encoder->isPasswordValid($this, $password, "");
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setEmail($email)
    {
        if (!$email) {
            return;
        }
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setRecoverPasswordToken($recoverPasswordToken)
    {
        $this->recoverPasswordToken = $recoverPasswordToken;

        if ($recoverPasswordToken) {
            $this->setRecoverPasswordTokenCreatedAt(new DateTime);
        } else {
            $this->setRecoverPasswordTokenCreatedAt(null);
        }

        return $this;
    }

    public function getRecoverPasswordToken()
    {
        return $this->recoverPasswordToken;
    }

    public function setRecoverPasswordTokenCreatedAt($recoverPasswordTokenCreatedAt)
    {
        $this->recoverPasswordTokenCreatedAt = $recoverPasswordTokenCreatedAt;

        return $this;
    }

    public function getRecoverPasswordTokenCreatedAt()
    {
        return $this->recoverPasswordTokenCreatedAt;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function delete()
    {
        $crap = substr(uniqid(), 0, 8);

        $this->setEnabled(false);
        $this->setEmail("$crap@anymore.com");
        $this->setPassword($crap);
        $this->setUsername($crap);
        $this->setDeleted(true);
    }

    public function setRegisterToken($registerToken)
    {
        $this->registerToken = $registerToken;

        return $this;
    }

    public function getRegisterToken()
    {
        return $this->registerToken;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    public function serialize()
    {
        return serialize(
            [
                $this->id,
                $this->username,
                $this->email,
                $this->password,
            ]
        );
    }

    // @see \Serializable::unserialize()
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password
            ) = unserialize($serialized);
    }

    public function changePasswords($password, $newPassword, $newPassword2, $encoder)
    {

        if ($password) {

            if (!$newPassword) {
                throw new BadPasswordsException;
            }

            if ($newPassword !== $newPassword2) {
                throw new BadPasswordsException;
            }

            $this->password = $encoder->encodePassword($this, $newPassword);

            return $this;
        }

        return false;
    }

    public function checkEnabled()
    {
        if (!$this->enabled || $this->deleted) {
            throw new AccessDeniedException;
        }
    }

    public function getRoles()
    {
        return $this->roles->toArray();
    }

    public function addRole($role)
    {
        $this->roles[] = $role;

        return $this;
    }

    public function removeRole($role)
    {
        $this->roles->removeElement($role);
    }

    public function getDoctrineRoles()
    {
        return $this->roles;
    }

    public function addGroup($group)
    {
        $this->groups[] = $group;

        return $this;
    }

    public function removeGroup($group)
    {

        $this->groups->removeElement($group);
    }

    public function getGroups()
    {
        return $this->groups;
    }

    public function hasGroup($group)
    {
        foreach ($this->groups as $key => $value) {
            if ($value->getGroup() === $group || $value === $group) {
                return true;
            }
        }

        return false;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function grantsRole($roles = [])
    {

        if (is_string($roles)) {
            $roles = [$roles];
        }

        $roleHierarchy = Symfony::get('nakima_user.security.roles')->getHierarchy();

        $list = $roleHierarchy->getReachableRoles($this->getRoles());
        $names = [];
        foreach ($list as $role) {
            $names[] = $role->getRole();
        }

        foreach ($roles as $role) {
            if ($role instanceOf BaseRole) {
                $role = $role->getRole();
            }
            if (in_array($role, $names)) {
                return true;
            }
        }

        return false;
    }

    public function grantsGroup($group)
    {
        return $this->hasGroup($group);
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        if (!$gender) {
            return;
        }
        $this->gender = $gender;

        return $this;
    }
}
