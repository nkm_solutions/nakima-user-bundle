<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Entity;

/**
 * @author xgonzalez
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @MappedSuperclass
 * @DoctrineAssert\UniqueEntity("name")
 */
class Group extends BaseEntity
{

    /**
     * @Column(type="string", length=24)
     */
    protected $name;

    public function __toString()
    {
        return $this->name;
    }

    public function getGroup()
    {
        return $this->name;
    }

    public function setGroup($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
