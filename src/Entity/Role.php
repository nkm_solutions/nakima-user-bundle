<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Entity;

/**
 * @author xgonzalez
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Symfony;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * @MappedSuperclass
 * @DoctrineAssert\UniqueEntity("name")
 */
class Role extends BaseEntity implements RoleInterface
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    public function __toString()
    {
        return $this->name;
    }

    public function getRole()
    {
        return $this->name;
    }

    public function setRole($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public static function loadRole($role)
    {
        return Symfony::getDoctrine()
            ->getRepository("UserBundle:Role")
            ->findOneByName($role);
    }
}
