<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseStatusEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class Gender extends BaseStatusEntity
{

    /**
     * @Column(type="string", length=64)
     * @Assert\Choice(
     *     choices = {"MALE", "FEMALE"},
     *     message = "Choose a valid gender."
     * )
     */
    protected $name;

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }

}
