<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\Utils\String\Text;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiPasswordRecoverController extends BaseController
{

    public function indexAction()
    {
        $this->checkMethod("POST");
        $email = $this->getParam('email');
        $user = $this->getRepo("UserBundle:User")->findOneBy(['email' => $email]);

        $this->assertTrue400($user, 'email');

        do {
            $token = Text::rstr(64);
        } while ($this->getRepo("UserBundle:User")->findOneBy(['recoverPasswordToken' => $token]));

        $user->setRecoverPasswordToken($token);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        if ($user->isEnabled()) {
            $this->container->get('nakima_user.email.password_recover')->send(
                $this,
                $user
            );
        } else {
            $this->container->get('nakima_user.email.registration_validate')->send(
                $this,
                $user
            );
        }

        return new JsonResponse(
            array(
                "email" => $email,
            )
        );
    }
}
