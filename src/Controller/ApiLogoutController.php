<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiLogoutController extends BaseController
{

    public function indexAction(Request $request)
    {

        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return new JsonResponse(array());
    }
}
