<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiUserMeController extends BaseController
{

    public function indexAction(Request $request)
    {
        $this->checkMethod("GET");

        $user = $this->checkUser();

        return new JsonResponse(
            [
                'user' => Doctrine::toArray($user),
            ]
        );
    }

}
