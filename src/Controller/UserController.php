<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class UserController extends BaseController
{

    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $template = $this->getParameter('nakima_user')['templates']['login'];

        return $this->render(
            $template,
            [
                'last_username' => $lastUsername,
                'error' => $error,
                'nakima' => $this->getContainer(),
            ]
        );
    }

    /**
     * @Template()
     */
    public function checkAction(Request $request)
    {
        return [];
    }

    /**
     * @Template()
     */
    public function logoutAction(Request $request)
    {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return $this->newJsonResponse(array());
    }

    public function registerAction(Request $request)
    {

        $template = $this->getParameter('nakima_user')['templates']["register"];

        return $this->render($template);
    }

    public function registerCompleteAction(Request $request)
    {
        $template = $this->getParameter('nakima_user')['templates']['register_complete'];

        return $this->render($template);
    }

    public function registerValidateAction(Request $request)
    {

        $token = $request->get('token');

        $success = false;

        $user = $this->getRepo("UserBundle:User")->findOneByRecoverPasswordToken($token);

        if ($user) {
            $ok = $user->getRecoverPasswordTokenCreatedAt()->getDiffInMinutes() < 60;

            if ($ok) {

                $user->setEnabled(true);
                $user->setRecoverPasswordToken(null);
                $success = true;

                $manager = $this->getDoctrine()->getManager();
                $manager->flush();

                $this->get('nakima_user.email.registration_complete')->send(
                    $this,
                    $user
                );
            }
        }

        $template = $this->getParameter('nakima_user')['templates']['register_validate'];

        return $this->render(
            $template,
            [
                'success' => $success,
            ]
        );
    }

    public function passwordRecoverAction(Request $request)
    {
        $template = $this->getParameter('nakima_user')['templates']['password_recover'];

        return $this->render($template);
    }

    public function passwordNewAction(Request $request)
    {
        $template = $this->getParameter('nakima_user')['templates']['password_new'];

        return $this->render(
            $template,
            [
                'token' => $request->get('token', '1234'),
            ]
        );
    }

    public function passwordRecoverCompleteAction(Request $request)
    {
        $template = $this->getParameter('nakima_user')['templates']['password_recover_complete'];

        return $this->render($template);
    }
}
