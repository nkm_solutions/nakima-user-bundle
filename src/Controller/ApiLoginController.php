<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ApiLoginController extends BaseController
{

    public function indexAction(Request $request)
    {
        $this->checkMethod("POST");

        $firewall = "api";

        $username = $this->getParam("username");
        $password = $this->getParam("password");

        $userRepo = $this->getRepo("UserBundle:User");
        $user = $userRepo->findOneByUsername($username);
        $user2 = $userRepo->findOneByEmail($username);

        if ($user) {
            $user = $user;
        } else {
            if ($user2) {
                $user = $user2;
            } else {
                $this->assertTrue401($user);
            }
        }
        $this->assertTrue401($user->getEnabled());
        $this->assertFalse401($user->getDeleted());
        $this->assertTrue401($user->checkPassword($password));

        $key = $this->getParameter('secret');
        $t = new RememberMeToken($user, $firewall, $key);
        $t = new UsernamePasswordToken($user, $password, $firewall, $user->getRoles());
        $session = $request->getSession();
        $session->set($firewall, serialize($t));
        $session->save();
        $this->get('security.token_storage')->setToken($t);

        $event = new InteractiveLoginEvent($request, $t);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        return new JsonResponse(
            [
                'user' => Doctrine::toArray($user, ['full' => true]),
            ]
        );
        //return $this->redirectToRoute('nakima_user_api_user_me', [], 301);
    }

    /**
     * @Template()
     */
    public function checkAction()
    {
        return [];
    }

}
