<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\String\Text;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ApiRegisterController extends BaseController
{

    public function indexAction(Request $request)
    {
        $this->checkMethod("POST");

        $username = $this->getParam('username', false);
        $password = $this->getParam('password', false);
        $password2 = $this->getParam('password2', false);
        $email = $this->getParam('email', false);
        $enabled = $this->optParam('__enabled', false);

        $user = $this->getRepo("UserBundle:User")->findOneByUsername($username);
        $this->addBadParamIfTrue($user, 'username');
        $user = $this->getRepo("UserBundle:User")->findOneByEmail($email);
        $this->addBadParamIfTrue($user, 'email');

        $this->addBadParamIfFalse($password === $password2, 'password2');
        $this->addBadParamIfFalse($password, 'password');

        $this->checkMissingParams();

        $user = new \UserBundle\Entity\User;

        $user->setUsername($username);
        $user->setEnabled($enabled);
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPassword($password);

        if (!$enabled) {
            $user->setRegisterToken(Text::rstr(64));
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        if (!$enabled) {
            $this->get('nakima_user.email.registration_validate')->send(
                $this,
                $user
            );
        }

        return new JsonResponse(
            [
                'user' => Doctrine::toArray($user),
            ]
        );
    }
}
