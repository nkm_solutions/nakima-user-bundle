<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiRegisterValidateController extends BaseController
{

    public function indexAction(Request $request)
    {

        $token = $this->getParam('token');

        $user = $this->getRepo("UserBundle:User")->findOneByRegisterToken($token);

        $this->assertTrue400($user, 'token');
        $this->assertFalse400($user->getEnabled(), 'token');

        $user->setRegisterToken(null);
        $user->setEnabled(true);

        $manager = $this->getDoctrine()->getManager();
        $manager->flush();

        return new JsonResponse(['DONE']);

        return $this->redirectToRoute('nakima_user_user_me');
    }
}
