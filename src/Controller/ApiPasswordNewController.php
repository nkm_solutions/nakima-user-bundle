<?php
declare(strict_types=1);
namespace Nakima\UserBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiPasswordNewController extends BaseController
{

    public function indexAction(Request $request)
    {

        $this->checkMethod("POST");

        $password = $this->getParam('password', false);
        $password2 = $this->getParam('password2', false);
        $email = $this->getParam('email', false);
        $token = $this->getParam('token', false);

        $user = $this->getRepo("UserBundle:User")->findOneBy(
            [
                'email' => $email,
                'recoverPasswordToken' => $token,
            ]
        );

        $this->addBadParamIfFalse($user, 'email');
        $this->addBadParamIfFalse($password === $password2, 'password2');
        $this->addBadParamIfFalse($password, 'password');
        $this->checkMissingParams();

        $this->addBadParamIfFalse($user->getRecoverPasswordTokenCreatedAt()->getDiffInMinutes() < 60, 'email');
        $this->checkMissingParams();

        $user->setPassword($password);
        $user->setRecoverPasswordToken(null);

        $this->get('nakima_user.email.password_recover_complete')->send(
            $this,
            $user
        );

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return new JsonResponse(
            [
                'user' => Doctrine::toArray($user),
            ]
        );
    }
}
